package com.telerikacademi.carsafe.auth;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Toast;

import com.telerikacademi.carsafe.navigation.ActivityNavigation;
import com.telerikacademi.carsafe.navigation.NavigationProviderImpl;
import com.telerikacademi.carsafe.views.MainActivity;
import com.telerikacademi.carsafe.R;
import com.telerikacademi.carsafe.models.dto.NetworkResponse;
import com.telerikacademi.carsafe.models.dto.RegistrationInfo;
import com.telerikacademi.carsafe.navigation.FragmentNavigation;
import com.telerikacademi.carsafe.services.AuthService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AuthActivity extends AppCompatActivity implements FragmentNavigation {

    private AuthService authService;
    private ActivityNavigation navigationProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);

        authService = AuthService.getAuthInstance();
        navigationProvider = new NavigationProviderImpl();

        if (savedInstanceState == null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.authContainer, new LoginFrament())
                    .commit();
        }
    }

    public void sendCredentials(String email, String password) {

        authService.authenticate(email, password).enqueue(new Callback<NetworkResponse>() {
            @Override
            public void onResponse(Call<NetworkResponse> call, Response<NetworkResponse> response) {
                if (response.headers().get("authToken") != null) {

                    String token = response.headers().get("authToken");
                    int userId = response.body().getUserId();
                    SharedPreferences preferences = getSharedPreferences("CarSafe", Context.MODE_PRIVATE);
                    preferences.edit().putString("AUTHTOKEN", token).putInt("USERID", userId).apply();
                    navigationProvider.navigateToActivity(AuthActivity.this, MainActivity.class);

                } else {
                    Toast.makeText(AuthActivity.this, "Wrong pasword or email", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<NetworkResponse> call, Throwable t) {
                t.printStackTrace();
                Toast.makeText(AuthActivity.this, "No connecton with the server", Toast.LENGTH_SHORT).show();
            }
        });

    }


    public void registerNewUser(String email, String password) {
        RegistrationInfo registrationInfo = new RegistrationInfo();
        registrationInfo.setEmail(email);
        registrationInfo.setPassword(password);
        authService.registerNewUser(registrationInfo).enqueue(new Callback<RegistrationInfo>() {
            @Override
            public void onResponse(Call<RegistrationInfo> call, Response<RegistrationInfo> response) {

            }

            @Override
            public void onFailure(Call<RegistrationInfo> call, Throwable t) {

            }
        });
    }


    @Override
    public void navigateToFragment(Fragment fragment, boolean addToBackstack) {
        FragmentTransaction transaction =
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.authContainer, fragment);

        if (addToBackstack) {
            transaction.addToBackStack(null);
        }

        transaction.commit();
    }
}