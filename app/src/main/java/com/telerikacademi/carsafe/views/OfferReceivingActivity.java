package com.telerikacademi.carsafe.views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.telerikacademi.carsafe.R;
import com.telerikacademi.carsafe.databinding.ActivityOfferReceivingBinding;
import com.telerikacademi.carsafe.databinding.ActivityRequestPolicyBinding;
import com.telerikacademi.carsafe.models.dto.PolicyInfoDto;
import com.telerikacademi.carsafe.viewmodels.ReceivedPolicyViewModel;

public class OfferReceivingActivity extends AppCompatActivity {

    private static final String EXTRA_TOKEN = "com.telerikacademi.carsafe.EXTRA_TOKEN";
    private static final String EXTRA_VEHICLE_REG_DATE = "com.telerikacademi.carsafe.EXTRA_VEHICLE_REG_DATE";
    private static final String EXTRA_VEHICLE_CUBIC_CAPACITY = "com.telerikacademi.carsafe.EXTRA_VEHICLE_CUBIC_CAPACITY";
    private static final String EXTRA_VEHICLE_MODEL = "com.telerikacademi.carsafe.EXTRA_VEHICLE_MODEL";
    private static final String EXTRA_VEHICLE_BRAND = "com.telerikacademi.carsafe.EXTRA_VEHICLE_BRAND";

    private ReceivedPolicyViewModel viewModel;
    private LinearLayout infoTextLayout, priceTextLayout;
    private Button acceptBtn, dismissBtn;
    private ProgressBar loadingProgressBar;
    private Double priceOffer;
    private TextView priceTextView, errorText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityOfferReceivingBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_offer_receiving);

        errorText = binding.receivedOfferErrorText;
        errorText.setVisibility(View.GONE);

        loadingProgressBar = binding.receivedOfferProgressBar;
        loadingProgressBar.setVisibility(View.VISIBLE);

        infoTextLayout = binding.receivedOfferTextFields;
        infoTextLayout.setVisibility(View.GONE);

        priceTextLayout = binding.requestOfferPriceLayout;
        priceTextLayout.setVisibility(View.GONE);

        acceptBtn = binding.receivedPolicyAcceptbtn;
        acceptBtn.setVisibility(View.GONE);

        priceTextView = binding.priceOfferTextView;

        Intent intent = this.getIntent();

        String brandName = intent.getStringExtra(EXTRA_VEHICLE_BRAND);
        String modelname = intent.getStringExtra(EXTRA_VEHICLE_MODEL);
        String cubics = intent.getStringExtra(EXTRA_VEHICLE_CUBIC_CAPACITY);
        String regDate = intent.getStringExtra(EXTRA_VEHICLE_REG_DATE);
        String authToken = intent.getStringExtra(EXTRA_TOKEN);

        PolicyInfoDto infoDto = new PolicyInfoDto();
        infoDto.setVehicleCubicCapacity(cubics);
        infoDto.setVechicleModel(modelname);
        infoDto.setVechicleBrand(brandName);
        infoDto.setVehicleRegDate(regDate);

        viewModel = ViewModelProviders.of(this).get(ReceivedPolicyViewModel.class);
        viewModel.getPriceOffer(authToken, infoDto);

        binding.receivedOfferBrand.setText(brandName);
        binding.receivedOfferModel.setText(modelname);
        binding.receivedOfferCuics.setText(cubics);
        binding.receivedOfferRegDate.setText(regDate);

        observeForPriceOffer();

        acceptBtn.setOnClickListener(v -> {
            Intent returnIntent = new Intent();
            returnIntent.putExtra("resultPrice", priceOffer);
            returnIntent.putExtra("authToken", authToken);
            setResult(Activity.RESULT_OK, returnIntent);
            finish();
        });

        binding.receivedPolicyDismissbtn.setOnClickListener(v -> {
            Intent returnIntent = new Intent();
            setResult(Activity.RESULT_CANCELED, returnIntent);
            finish();
        });


    }

    private void observeForPriceOffer() {
        viewModel.offerPrice.observe(this, receivedPrice -> {
            if (receivedPrice != null) {
                priceOffer = receivedPrice;
                priceTextView.setText(Double.toString(priceOffer));
                loadingProgressBar.setVisibility(View.GONE);
                infoTextLayout.setVisibility(View.VISIBLE);
                priceTextLayout.setVisibility(View.VISIBLE);
                acceptBtn.setVisibility(View.VISIBLE);

            }
        });

        viewModel.loadingError.observe(this, isError -> {
            if (isError != null) {
                errorText.setVisibility(isError ? View.VISIBLE : View.GONE);
            }
        });

        viewModel.isLoading.observe(this, isLoading -> {
            if (isLoading != null) {
                loadingProgressBar.setVisibility(isLoading ? View.VISIBLE : View.GONE);
                if (isLoading) {
                    loadingProgressBar.setVisibility(View.VISIBLE);
                    infoTextLayout.setVisibility(View.GONE);
                    priceTextLayout.setVisibility(View.GONE);
                    acceptBtn.setVisibility(View.GONE);
                }
            }
        });

    }
}