package com.telerikacademi.carsafe.views;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.telerikacademi.carsafe.R;
import com.telerikacademi.carsafe.models.dto.PolicyInfoDto;

public class PolicyListAdapter extends ListAdapter<PolicyInfoDto, PolicyListAdapter.PolicyViewHolder> {

    Context context;
    private AdapterClickListener adapterClickListener;

    public PolicyListAdapter(Context context) {
        super(DIFF_CALLBACK);
        this.context = context;
    }

    private static final DiffUtil.ItemCallback<PolicyInfoDto> DIFF_CALLBACK = new DiffUtil.ItemCallback<PolicyInfoDto>() {
        @Override
        public boolean areItemsTheSame(@NonNull PolicyInfoDto oldItem, @NonNull PolicyInfoDto newItem) {
            return oldItem.getPolicyId() == newItem.getPolicyId();
        }

        @Override
        public boolean areContentsTheSame(@NonNull PolicyInfoDto oldItem, @NonNull PolicyInfoDto newItem) {
            return oldItem.getStartDate().equals(newItem.getStartDate());
        }
    };

    @NonNull
    @Override
    public PolicyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.policy_item, parent, false);
        return new PolicyViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull PolicyViewHolder holder, int position) {

        PolicyInfoDto currentPolicy = getItem(position);

        holder.vehicleRegDate.setText(currentPolicy.getVehicleRegDate());
        holder.vehicleModel.setText(currentPolicy.getVechicleBrand() + " " + currentPolicy.getVechicleModel());
        holder.startDate.setText(currentPolicy.getStartDate());
        holder.startTime.setText(currentPolicy.getStartTime());
        holder.policyPrice.setText(Double.toString(currentPolicy.getPolicyPrice()));
        holder.policyId.setText(currentPolicy.getPolicyId() + " / " + currentPolicy.getStartDate());
        holder.policyApprovalStatusImage.setImageDrawable(drawable(currentPolicy.getPolicyApprovalStatus()));

    }

    private Drawable drawable (int approvalStatus){
        switch (approvalStatus) {
            case 0:
                return ContextCompat.getDrawable(context, R.drawable.ic_pending);
            case 1:
                return ContextCompat.getDrawable(context, R.drawable.ic_approved);
            case 2:
                return ContextCompat.getDrawable(context, R.drawable.ic_rejected);
            case 3:
                return ContextCompat.getDrawable(context, R.drawable.ic_withdrawn);
            default:
                return ContextCompat.getDrawable(context, R.drawable.ic_error_image);
        }
    }

    class PolicyViewHolder extends RecyclerView.ViewHolder {

        private TextView policyId, policyPrice, startTime, startDate, vehicleModel, vehicleRegDate;
        private ImageView policyApprovalStatusImage;

        public PolicyViewHolder(@NonNull View itemView) {
            super(itemView);

            policyId = itemView.findViewById(R.id.policyIdentityTextField);
            policyPrice = itemView.findViewById(R.id.policyPremiumTextField);
            startDate = itemView.findViewById(R.id.policyStartDateTextField);
            startTime = itemView.findViewById(R.id.policyStartTimeTextField);
            vehicleModel = itemView.findViewById(R.id.policyVehicleModelTextField);
            vehicleRegDate = itemView.findViewById(R.id.policyVehicleRegDateTextField);
            policyApprovalStatusImage = itemView.findViewById(R.id.policyStatusImage);

            itemView.setOnClickListener(view-> {
                int position = getAdapterPosition();
                if (adapterClickListener != null && position != RecyclerView.NO_POSITION) {
                    adapterClickListener.onAdapterClick(getItem(position));
                }
            });

        }
    }

    public interface AdapterClickListener {
        void onAdapterClick(PolicyInfoDto policyInfo);
    }

    public void setAdapterClickListener(AdapterClickListener adapterClickListener){
        this.adapterClickListener = adapterClickListener;
    }
}
