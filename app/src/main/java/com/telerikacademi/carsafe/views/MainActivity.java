package com.telerikacademi.carsafe.views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.telerikacademi.carsafe.R;
import com.telerikacademi.carsafe.databinding.ActivityMainBinding;
import com.telerikacademi.carsafe.models.dto.NetworkResponse;
import com.telerikacademi.carsafe.models.dto.PolicyInfoDto;
import com.telerikacademi.carsafe.models.dto.UserProfileInfoDto;
import com.telerikacademi.carsafe.services.AuthService;
import com.telerikacademi.carsafe.viewmodels.UserProfileViewModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private static final String EXTRA_ID = "com.telerikacademi.carsafe.EXTRA_ID";
    private static final String EXTRA_TOKEN = "com.telerikacademi.carsafe.EXTRA_TOKEN";
    private static final String EXTRA_APPROVAL_STATUS = "com.telerikacademi.carsafe.EXTRA_APPROVAL_STATUS";
    private static final String EXTRA_POLICY_PRICE = "com.telerikacademi.carsafe.EXTRA_POLICY_PRICE";
    private static final String EXTRA_START_DATE = "com.telerikacademi.carsafe.EXTRA_START_DATE";
    private static final String EXTRA_START_TIME = "com.telerikacademi.carsafe.EXTRA_START_TIME";
    private static final String EXTRA_VEHICLE_MODEL = "com.telerikacademi.carsafe.EXTRA_VEHICLE_MODEL";
    private static final String EXTRA_VEHICLE_BRAND = "com.telerikacademi.carsafe.EXTRA_VEHICLE_BRAND";
    private static final String EXTRA_VEHICLE_REG_DATE = "com.telerikacademi.carsafe.EXTRA_VEHICLE_REG_DATE";
    private static final String EXTRA_VEHICLE_CUBIC_CAPACITY = "com.telerikacademi.carsafe.EXTRA_VEHICLE_CUBIC_CAPACITY";
    private static final String FIRST_NAME = "com.telerikacademi.carsafe.FIRST_NAME";
    private static final String LAST_NAME = "com.telerikacademi.carsafe.LAST_NAME";
    private static final String BIRTH_DATE = "com.telerikacademi.carsafe.BIRTH_DATE";
    private static final String ACCIDENTS = "com.telerikacademi.carsafe.ACCIDENTS";


    private TextView firstNameField, lastNameField, dateOfBirthField, emailAddressField,
            addressField, phoneField, policyErrorLoadingText;

    private RecyclerView policiesRecyclerView;
    private UserProfileViewModel profileViewModel;
    private PolicyListAdapter policyListAdapter;
    private LinearLayout noInfoErrrorBlock, mainView,mainViewText;
    private ProgressBar mainLoadingLayout;
    private Button requestNewPolicyButton;
    private SwipeRefreshLayout policiesRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityMainBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        policyListAdapter = new PolicyListAdapter(this);

        mainLoadingLayout = binding.mainLoadingProgressBar;
        mainView = binding.mainLayout;
        mainViewText = binding.mainProfileInfoLayoutText;

        firstNameField = binding.profileFirstName;
        lastNameField = binding.profileLastName;
        dateOfBirthField = binding.profileBirthDate;
        emailAddressField = binding.profileEmail;
        addressField = binding.profileAddress;
        phoneField = binding.profilePhone;
        requestNewPolicyButton = binding.requestPolicyButton;
        policiesRefreshLayout = binding.policyRefreshLayout;

        policyErrorLoadingText = binding.policyLoadingErrorText;
        policyErrorLoadingText.setVisibility(View.GONE);

        noInfoErrrorBlock = binding.noIfnoErrorblock;
        noInfoErrrorBlock.setVisibility(View.GONE);

        SharedPreferences prefs = this.getSharedPreferences("CarSafe", Context.MODE_PRIVATE);
        String authToken = "Bearer " + prefs.getString("AUTHTOKEN", "AuthToken missing");
        int userId = prefs.getInt("USERID", -1);

        profileViewModel = ViewModelProviders.of(this).get(UserProfileViewModel.class);
        profileViewModel.getUserPoliciesList(userId, authToken);
        profileViewModel.getUserProfileInfo(userId, authToken);

        oberveUserInfo();
        observeUsersPolicies();

        policiesRecyclerView = binding.policyRecyclerList;
        policiesRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        policiesRecyclerView.addItemDecoration(new DividerItemDecoration(this,
                DividerItemDecoration.VERTICAL));
        policiesRecyclerView.setAdapter(policyListAdapter);


        policyListAdapter.setAdapterClickListener(policyInfo -> {
            Intent intent = new Intent(MainActivity.this, PolicyInfoActivity.class);
            intent.putExtra(EXTRA_ID, policyInfo.getPolicyId());
            intent.putExtra(EXTRA_APPROVAL_STATUS, policyInfo.getPolicyApprovalStatus());
            intent.putExtra(EXTRA_POLICY_PRICE, policyInfo.getPolicyPrice());
            intent.putExtra(EXTRA_START_DATE, policyInfo.getStartDate());
            intent.putExtra(EXTRA_START_TIME, policyInfo.getStartTime());
            intent.putExtra(EXTRA_VEHICLE_MODEL, policyInfo.getVechicleModel());
            intent.putExtra(EXTRA_VEHICLE_BRAND, policyInfo.getVechicleBrand());
            intent.putExtra(EXTRA_VEHICLE_REG_DATE, policyInfo.getVehicleRegDate());
            intent.putExtra(EXTRA_VEHICLE_CUBIC_CAPACITY, policyInfo.getVehicleCubicCapacity());
            startActivity(intent);
        });

        policiesRefreshLayout.setOnRefreshListener(() -> {
            profileViewModel.getUserPoliciesList(userId, authToken);
            observeUsersPolicies();

            policiesRefreshLayout.setRefreshing(false);
        });

        binding.mainActivityEditProfileButton.setOnClickListener( v -> {
            Intent intent = new Intent(MainActivity.this, EditProfileActivity.class);
            intent.putExtra(EXTRA_ID, userId);
            intent.putExtra(EXTRA_TOKEN, authToken);
            startActivity(intent);
        });

        binding.requestPolicyButton.setOnClickListener( v -> {
            Intent intent = new Intent(MainActivity.this, RequestPolicyActivity.class);
            intent.putExtra(EXTRA_ID, userId);
            intent.putExtra(EXTRA_TOKEN, authToken);
            startActivity(intent);
        });
    }

    private void oberveUserInfo() {
        profileViewModel.loadingUserInfoError.observe(this, noUserInfoAvailable -> {
            if (noUserInfoAvailable) {
                requestNewPolicyButton.setVisibility(View.GONE);
                mainLoadingLayout.setVisibility(View.GONE);
                noInfoErrrorBlock.setVisibility(View.VISIBLE);
                mainLoadingLayout.setVisibility(View.VISIBLE);
            }
        });

        profileViewModel.loadingUserInfo.observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean isLoading) {
                if (isLoading != null) {
                    mainLoadingLayout.setVisibility(isLoading ? View.VISIBLE : View.GONE);
                    if (isLoading) {
                        mainView.setVisibility(View.GONE);
                        mainViewText.setVisibility(View.GONE);
                        noInfoErrrorBlock.setVisibility(View.GONE);
                    }
                }
            }
        });
        profileViewModel.userProfileInfo.observe(this, userProfileInfoDto -> {
            if (userProfileInfoDto.getFirstname() != null) {
                firstNameField.setText(userProfileInfoDto.getFirstname());
                lastNameField.setText(userProfileInfoDto.getLastname());
                dateOfBirthField.setText(userProfileInfoDto.getBirthdate());
                addressField.setText(userProfileInfoDto.getAddress());
                emailAddressField.setText(userProfileInfoDto.getEmail());
                phoneField.setText(userProfileInfoDto.getPhoneNumber());

                mainView.setVisibility(View.VISIBLE);
                mainViewText.setVisibility(View.VISIBLE);
                mainLoadingLayout.setVisibility(View.GONE);
                noInfoErrrorBlock.setVisibility(View.GONE);

                SharedPreferences preferences = getSharedPreferences("PolicyInfo", Context.MODE_PRIVATE);
                preferences.edit()
                        .putString(FIRST_NAME, userProfileInfoDto.getFirstname())
                        .putString(LAST_NAME, userProfileInfoDto.getLastname())
                        .putString(BIRTH_DATE, userProfileInfoDto.getBirthdate())
                        .putString(ACCIDENTS, userProfileInfoDto.getAccidentLastYear())
                        .apply();
            }
        });
    }

    private void observeUsersPolicies() {
        profileViewModel.userPoliciesList.observe(this, policyInfoDtoList -> {
            if (policyInfoDtoList == null ) {
                policyErrorLoadingText.setVisibility(View.VISIBLE);
                policiesRefreshLayout.setVisibility(View.GONE);
            }

            if (policyInfoDtoList.isEmpty()) {
                policyErrorLoadingText.setVisibility(View.VISIBLE);
                policiesRefreshLayout.setVisibility(View.GONE);
            }

            if (policyInfoDtoList != null) {
                policyListAdapter.notifyDataSetChanged();
                policyListAdapter.submitList(policyInfoDtoList);
            }
        });
    }
}