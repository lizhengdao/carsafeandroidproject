package com.telerikacademi.carsafe.viewmodels;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.telerikacademi.carsafe.models.dto.PolicyInfoDto;
import com.telerikacademi.carsafe.services.PoliciesService;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class ReceivedPolicyViewModel extends ViewModel {

    public MutableLiveData<Double> offerPrice = new MutableLiveData<Double>();
    public MutableLiveData<Boolean> isLoading = new MutableLiveData<Boolean>();
    public MutableLiveData<Boolean> loadingError = new MutableLiveData<Boolean>();

    private PoliciesService policiesService;

    public ReceivedPolicyViewModel() {
        policiesService = PoliciesService.getInstance();
    }

    private CompositeDisposable disposable = new CompositeDisposable();

    public void getPriceOffer(String token, PolicyInfoDto infoDto) {
        isLoading.setValue(true);
        disposable.add(policiesService.getOfferPrice(token, infoDto).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<Double>() {
                    @Override
                    public void onSuccess(Double receivedOfferPrice) {
                        offerPrice.setValue(receivedOfferPrice);
                        loadingError.setValue(false);
                        isLoading.setValue(false);
                    }

                    @Override
                    public void onError(Throwable e) {
                        isLoading.setValue(false);
                        loadingError.setValue(true);
                        e.printStackTrace();
                    }
                })
        );

    }

    @Override
    protected void onCleared() {
        super.onCleared();
        disposable.clear();
    }
}
