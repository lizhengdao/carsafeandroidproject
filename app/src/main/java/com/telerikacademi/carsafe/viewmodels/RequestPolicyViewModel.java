package com.telerikacademi.carsafe.viewmodels;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.telerikacademi.carsafe.models.CarBrand;
import com.telerikacademi.carsafe.models.CarModel;
import com.telerikacademi.carsafe.models.dto.PolicyInfoDto;
import com.telerikacademi.carsafe.services.VehicleService;

import java.util.List;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class RequestPolicyViewModel extends ViewModel {

    public MutableLiveData<List<CarBrand>> carBrandsList = new MutableLiveData<List<CarBrand>>();
    public MutableLiveData<List<CarModel>> carModelsList = new MutableLiveData<List<CarModel>>();
    public MutableLiveData<List<Integer>> carCubicsList = new MutableLiveData<List<Integer>>();

    private VehicleService vehicleService;

    public RequestPolicyViewModel() {
        vehicleService = VehicleService.getInstance();
    }

    public void getCarBrands(String authToken){
        observeDataFromNetwork(vehicleService.getCarBrands(authToken), carBrandsList );
    }

    public void getCarModelsForBrand(String authToken, int brandId){
        observeDataFromNetwork(vehicleService.getCarModelsForBrand(brandId, authToken), carModelsList);
    }

    public void getMaxCarCubics(String token){
        observeDataFromNetwork(vehicleService.getMaxCubicsValues(token), carCubicsList);
    }


    private CompositeDisposable disposable = new CompositeDisposable();

    private <E> void observeDataFromNetwork(Single<E> observable, MutableLiveData<E> liveData){
        disposable.add(

                observable.subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new DisposableSingleObserver<E>() {
                            @Override
                            public void onSuccess(E e) {
                                liveData.setValue(e);
                            }

                            @Override
                            public void onError(Throwable e) {
                            }
                        })
        );
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        disposable.clear();
    }
}
