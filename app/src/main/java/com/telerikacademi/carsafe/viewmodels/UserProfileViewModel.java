package com.telerikacademi.carsafe.viewmodels;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.telerikacademi.carsafe.models.dto.PolicyInfoDto;
import com.telerikacademi.carsafe.models.dto.UserProfileInfoDto;
import com.telerikacademi.carsafe.services.UserInfoService;

import java.util.List;
import java.util.Observable;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class UserProfileViewModel extends ViewModel {

    public MutableLiveData<List<PolicyInfoDto>> userPoliciesList = new MutableLiveData<List<PolicyInfoDto>>();
    public MutableLiveData<UserProfileInfoDto> userProfileInfo = new MutableLiveData<UserProfileInfoDto>();
    public MutableLiveData<Boolean> loadingUserInfo = new MutableLiveData<Boolean>();
    public MutableLiveData<Boolean> loadingUserPolicies = new MutableLiveData<Boolean>();
    public MutableLiveData<Boolean> loadingUserInfoError = new MutableLiveData<Boolean>();
    public MutableLiveData<Boolean> loadingUserPoliciesError = new MutableLiveData<Boolean>();

    UserInfoService userInfoService;

    public UserProfileViewModel() {
        userInfoService = UserInfoService.getInstance();
    }

    public void getUserProfileInfo(int userId, String authToken) {
        observeDataFromNetwork(userInfoService.getUserInfo(userId, authToken), userProfileInfo, loadingUserInfo, loadingUserInfoError);
    }

    public void getUserPoliciesList(int userId, String authToken) {
        observeDataFromNetwork(userInfoService.getUserPolicies(userId, authToken), userPoliciesList, loadingUserPolicies, loadingUserPoliciesError);
    }

    private CompositeDisposable disposable = new CompositeDisposable();

    private <E> void observeDataFromNetwork(Single<E> observable, MutableLiveData<E> liveData,
                                            MutableLiveData<Boolean> loading, MutableLiveData<Boolean> loadingError) {
        loading.setValue(true);
        disposable.add(
                observable.subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new DisposableSingleObserver<E>() {
                            @Override
                            public void onSuccess(E e) {
                                liveData.setValue(e);
                                loadingError.setValue(false);
                                loading.setValue(false);
                            }

                            @Override
                            public void onError(Throwable e) {
                                loadingError.setValue(true);
                                loading.setValue(false);
                            }
                        })
        );
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        disposable.clear();
    }
}
