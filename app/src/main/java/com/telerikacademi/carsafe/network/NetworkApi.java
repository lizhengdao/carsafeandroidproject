package com.telerikacademi.carsafe.network;

import android.net.Network;

import com.telerikacademi.carsafe.models.CarBrand;
import com.telerikacademi.carsafe.models.CarModel;
import com.telerikacademi.carsafe.models.dto.NetworkResponse;
import com.telerikacademi.carsafe.models.dto.PolicyInfoDto;
import com.telerikacademi.carsafe.models.dto.RegistrationInfo;
import com.telerikacademi.carsafe.models.dto.UserProfileInfoDto;

import java.util.List;

import io.reactivex.Single;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface NetworkApi {

    @GET("/v.1.0/api/user/authentication")
    Call<NetworkResponse> authenticate(
            @Header("userMail") String userEmail,
            @Header("userPass") String userPassword
    );

    @POST("/v.1.0/api/user/registration")
    Call<RegistrationInfo> registerNewUser(
            @Body RegistrationInfo registrationInfo
    );

    @GET("/v.1.0/api/user/profile/{user-id}")
    Single<UserProfileInfoDto> getProfileDetails(
            @Header("Authorization") String token,
            @Path("user-id") int userId
    );

    @GET("/v.1.0/api/policy/profile/{user-id}")
    Single<List<PolicyInfoDto>> getUserPolicies(
            @Header("Authorization") String token,
            @Path("user-id") int userId
    );

    @PUT("/v.1.0/api/policy/withdrawn/{policy-id}")
    Call<PolicyInfoDto> withdrawPendingPolicy(
            @Header("Authorization") String token,
            @Path("policy-id") int policyId);

    @GET("/v.1.0/api/car/brands")
    Single<List<CarBrand>> getAllVehicleBrands(
            @Header("Authorization") String token);

    @GET("/v.1.0/api/car/models/brand/{brand-id}")
    Single<List<CarModel>> getAllModelsForBrand(
            @Header("Authorization") String token,
            @Path("brand-id") int brandId
    );

    @PUT("/v.1.0/api/user/profile/{user-id}")
    Call<UserProfileInfoDto> updateProfileInfo(
            @Header("Authorization") String token,
            @Path("user-id") int userId,
            @Body UserProfileInfoDto infoDto
    );

    @POST("/v.1.0/api/policy")
    Call<PolicyInfoDto> registerNewPolicy(
            @Header("Authorization") String token,
            @Body PolicyInfoDto infoDto
    );

    @GET("/v.1.0/api/offer/maxcubics")
    Single<List<Integer>> getCarMaxCubics(@Header("Authorization") String token);

    @POST("/v.1.0/api/offer")
    Single<Double> getOfferPrice(@Header("Authorization") String token,
                                 @Body PolicyInfoDto infoDto);

}
