package com.telerikacademi.carsafe.network;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class NetworkProvider {

    public static final String BASE_URL = "http://fbe44c48bba1.ngrok.io/";

    public NetworkApi getConnection(){
        return api;
    }

    private NetworkApi api = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
            .create(NetworkApi.class);

}
