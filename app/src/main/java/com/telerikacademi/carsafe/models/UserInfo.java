package com.telerikacademi.carsafe.models;

import java.util.Set;

public class UserInfo {

    private int id;

    private String firstName;

    private String lastName;

    private String email;

    private String phoneNumber;

    private String address;

    private String password;

    private String birthDate;

    private byte previousAccident;

    private String userRole;

    private String identificationToken;

    private Set<Policy> userPoliciesSet;

    private Set<Car> usersCars;

    public UserInfo() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public byte getPreviousAccident() {
        return previousAccident;
    }

    public void setPreviousAccident(byte previousAccident) {
        this.previousAccident = previousAccident;
    }

    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }

    public String getIdentificationToken() {
        return identificationToken;
    }

    public void setIdentificationToken(String identificationToken) {
        this.identificationToken = identificationToken;
    }

    public Set<Policy> getUserPoliciesSet() {
        return userPoliciesSet;
    }

    public void setUserPoliciesSet(Set<Policy> userPoliciesSet) {
        this.userPoliciesSet = userPoliciesSet;
    }

    public Set<Car> getUsersCars() {
        return usersCars;
    }

    public void setUsersCars(Set<Car> usersCars) {
        this.usersCars = usersCars;
    }
}
