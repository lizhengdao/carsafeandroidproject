package com.telerikacademi.carsafe.models.dto;

public class UserProfileInfoDto {

    private String firstname;
    private String lastname;
    private String birthdate;
    private String email;
    private String phoneNumber;
    private String address;
    private String accidentLastYear;

    public UserProfileInfoDto() {}

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public String getEmail() {
        return email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public String getAccidentLastYear() {
        return accidentLastYear;
    }

    public void setAccidentLastYear(String accidentLastYear) {
        this.accidentLastYear = accidentLastYear;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setAddress(String address) {
        this.address = address;
    }

}
