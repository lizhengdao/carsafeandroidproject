package com.telerikacademi.carsafe.models;


import androidx.annotation.NonNull;

public class CarModel {

    private int id;

    private String model;

    private CarBrand carBrand;

    public CarModel() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public CarBrand getCarBrand() {
        return carBrand;
    }

    public void setCarBrand(CarBrand carBrand) {
        this.carBrand = carBrand;
    }

    @NonNull
    @Override
    public String toString() {
        return model;
    }
}
