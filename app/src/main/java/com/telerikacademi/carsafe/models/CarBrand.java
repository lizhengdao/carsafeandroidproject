package com.telerikacademi.carsafe.models;

import androidx.annotation.NonNull;

import java.util.Set;

public class CarBrand {

    private int id;

    private String brand;

    private Set<CarModel> carModelSet;

    public CarBrand() {
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public Set<CarModel> getCarModelSet() {
        return carModelSet;
    }

    public void setCarModelSet(Set<CarModel> carModelSet) {
        this.carModelSet = carModelSet;
    }

    @NonNull
    @Override
    public String toString() {
        return brand;
    }
}
