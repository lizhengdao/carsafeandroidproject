package com.telerikacademi.carsafe.models;

public class Car {

    private int id;

    private String registrationDate;

    private String cubicCapacity;

    private CarModel carModel;

    private UserInfo carOwner;

    public Car() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(String registrationDate) {
        this.registrationDate = registrationDate;
    }

    public String getCubicCapacity() {
        return cubicCapacity;
    }

    public void setCubicCapacity(String cubicCapacity) {
        this.cubicCapacity = cubicCapacity;
    }

    public CarModel getCarModel() {
        return carModel;
    }

    public void setCarModel(CarModel carModel) {
        this.carModel = carModel;
    }

    public UserInfo getCarOwner() {
        return carOwner;
    }

    public void setCarOwner(UserInfo carOwner) {
        this.carOwner = carOwner;
    }
}
