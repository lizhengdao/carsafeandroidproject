package com.telerikacademi.carsafe.navigation;

import android.content.Context;
import android.content.Intent;

public class NavigationProviderImpl implements ActivityNavigation {

    @Override
    public void navigateToActivity(Context context, Class<?> calledActivity) {
        Intent intent = new Intent(context, calledActivity);
        context.startActivity(intent);
    }
}
