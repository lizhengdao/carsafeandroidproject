package com.telerikacademi.carsafe.navigation;

import android.content.Context;

public interface ActivityNavigation {

    void navigateToActivity(Context context, Class<?> calledActivity);


}
