package com.telerikacademi.carsafe.services;

import com.telerikacademi.carsafe.models.dto.PolicyInfoDto;
import com.telerikacademi.carsafe.models.dto.UserProfileInfoDto;
import com.telerikacademi.carsafe.network.NetworkProvider;

import java.util.List;

import io.reactivex.Single;
import retrofit2.Call;

public class UserInfoService {

    private static UserInfoService instance;
    private NetworkProvider networkProvider;

    private UserInfoService() {
        networkProvider = new NetworkProvider();
    }

    public static UserInfoService getInstance() {
        if (instance == null) {
            instance = new UserInfoService();
        }
        return instance;
    }

    public Single<UserProfileInfoDto> getUserInfo(int userId, String authToken) {
        return networkProvider.getConnection().getProfileDetails(authToken, userId);
    }

    public Single<List<PolicyInfoDto>> getUserPolicies (int userId, String authToken) {
        return networkProvider.getConnection().getUserPolicies(authToken, userId);
    }

    public Call<UserProfileInfoDto> updateUserProfileInfo (String token, int userId, UserProfileInfoDto infoDto){
        return  networkProvider.getConnection().updateProfileInfo(token, userId, infoDto);
    }
}
