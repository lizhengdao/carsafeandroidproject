package com.telerikacademi.carsafe.services;

import com.telerikacademi.carsafe.models.CarBrand;
import com.telerikacademi.carsafe.models.CarModel;
import com.telerikacademi.carsafe.network.NetworkProvider;

import java.util.List;

import io.reactivex.Single;

public class VehicleService {

    private static VehicleService instance;
    private NetworkProvider networkProvider;

    private VehicleService() {
        networkProvider = new NetworkProvider();
    }

    public static VehicleService getInstance() {
        if (instance == null) {
            instance = new VehicleService();
        }
        return instance;
    }

    public Single<List<CarBrand>> getCarBrands(String authToken) {
        return networkProvider.getConnection().getAllVehicleBrands(authToken);
    }

    public Single<List<CarModel>> getCarModelsForBrand (int brandId, String authToken) {
        return networkProvider.getConnection().getAllModelsForBrand(authToken, brandId);
    }

    public Single<List<Integer>> getMaxCubicsValues(String token){
        return networkProvider.getConnection().getCarMaxCubics(token);
    }

}
