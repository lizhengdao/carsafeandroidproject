# CarSafe 

### CarSafe is a mobile app for Android. It was developed as an addition to the Car Safe web project for Telerik Academy.

## Table of Content

- [General Info](#general-info)
- [Technologies](#technologies)
- [Features](#features)

## General Info
CarSafe was developed as an addition to the web project Safty Car for Telerik Academy. The main purpose of the application is to demonstrate how the users of the web application can have more flexibility in requesting and managing their auto insurance policies htrough their mobile phones.



## Technologies
The tech stack includes:
* MVVM architecture
* Retrofit for communication with the REST API
* Data Binding
* JWT Authentication

## Features
 
 ### - Authentication:
 The application supports user registration and authentication with JWT.


<table style="background-color: #708090;">
<tr>
<th>Login Layout :</th>
<th>Register Layout :</th>
</tr>
<td style="background-color: #708090;"><img src="screenshots/CarSafeLoginFin.png" alt="login layout" height="350"/></td>
<td style="background-color: #708090;"><img src="screenshots/CarSafeRegisterFin.png" alt="register layout" height="350"/></td>
</table>


 ### - User Related Information:
 The application provides information about the user profile as well as a history of the insurance policy requests made by the user. <br>
 The history is loaded in a Recycler View with different icons indicating the status of the policy: pending, approved, declined, or withdrawn.<br>The user can also click on a policy from the Recycler View to see it in more details. If the status of the policy is still pending, the user can choose to withdraw it.

 <table style="background-color: #708090;">
<tr>
<th>User Profile Layout Without History :</th>
<th>User Profile Layout With History :</th>
<th>Policy Details Layout :</th>
</tr>
<td style="background-color: #708090;"><img src="screenshots/CarSafeMainViewEmptyPoliciesListFin.png" alt="user profile layout" height="350"/></td>
<td style="background-color: #708090;"><img src="screenshots/CarSafeMainViewWithPoliciesFin.png" alt="user profile layout" height="350"/></td>
<td style="background-color: #708090;"><img src="screenshots/CarSafePolicyInfoLayoutFin.png" alt="policy details layout" height="350"/></td>
</table>

### - Requesting A New Auto Insurance Policy:
 The application provides functionality that enables the user to request a new auto insurance policy from his phone.

 <table style="background-color: #708090;">
<tr>
<th>Policy Request Layout :</th>
<th>Response layout :</th>
</tr>
<td style="background-color: #708090;"><img src="screenshots/CarSafeRequestScreenFin.png" alt="user profile layout" height="350"/></td>
<td style="background-color: #708090;"><img src="screenshots/CarSafeOfferResponseFin.png" alt="user profile layout"  height="350"/></td>
</table>
